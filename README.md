# Type Converter

Módulo para conversão de tipos onde se precisa obter um determinado tipo independente do valor passado como parâmetro. Nos casos onde a conversão não é possível a emissão do erro é evitada e um valor padrão é retornado.


## Exemplo

```
#! php

require 'vendor/autoload.php';

use Ciebit\TypeConverter;


$number01 = TypeConverter::toInt('569');
$number02 = TypeConverter::toInt(function(){});

// $number01 será igual a 569
// $number02 será igual a 0 pois não foi possível a conversão

```

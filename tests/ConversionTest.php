<?php
use Ciebit\TypeConverter;
use PHPUnit\Framework\TestCase;

class ConversionTest extends TestCase
{
    public function testFloat()
    {
        $this->assertEquals(2.0, TypeConverter::toFloat(2));
        $this->assertEquals(3.4, TypeConverter::toFloat(3.4));
        $this->assertEquals(5.0, TypeConverter::toFloat('5'));
        $this->assertEquals(0.0, TypeConverter::toFloat('a6'));
        $this->assertEquals(0.0, TypeConverter::toFloat('abcdefg'));
        $this->assertEquals(0.0, TypeConverter::toFloat(new DateTime));
        $this->assertEquals(0.0, TypeConverter::toFloat([]));
        $this->assertEquals(0.0, TypeConverter::toFloat([1, 2, 3]));
        $this->assertEquals(0.0, TypeConverter::toFloat(function(){}));
    }

    public function testInt()
    {
        $this->assertEquals(2, TypeConverter::toInt(2));
        $this->assertEquals(3, TypeConverter::toInt(3.4));
        $this->assertEquals(5, TypeConverter::toInt('5'));
        $this->assertEquals(0, TypeConverter::toInt('a6'));
        $this->assertEquals(0, TypeConverter::toInt('abcdefg'));
        $this->assertEquals(0, TypeConverter::toInt(new DateTime));
        $this->assertEquals(0, TypeConverter::toInt([]));
        $this->assertEquals(0, TypeConverter::toInt([1, 2, 3]));
        $this->assertEquals(0, TypeConverter::toInt(function(){}));
    }

    public function testString()
    {
        $this->assertEquals('2', TypeConverter::toString(2));
        $this->assertEquals('3.4', TypeConverter::toString(3.4));
        $this->assertEquals('5', TypeConverter::toString('5'));
        $this->assertEquals('a6', TypeConverter::toString('a6'));
        $this->assertEquals('abcdefg', TypeConverter::toString('abcdefg'));
        $this->assertEquals('', TypeConverter::toString(new DateTime));
        $this->assertEquals('', TypeConverter::toString([]));
        $this->assertEquals('', TypeConverter::toString([1, 2, 3]));
        $this->assertEquals('', TypeConverter::toString(function(){}));
    }
}

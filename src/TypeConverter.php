<?php
namespace Ciebit;

class TypeConverter
{
    public static function toFloat($value): float
    {
        if (is_object($value) || is_array($value)) {
            return 0.0;
        }

        return floatval($value);
    }

    public static function toInt($value): int
    {
        if (is_object($value) || is_array($value)) {
            return 0;
        }

        return intval($value);
    }

    public static function toString($value): string
    {
        if (is_object($value) || is_array($value)) {
            return '';
        }

        return strval($value);
    }
}
